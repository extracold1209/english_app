declare module 'react-native-tts' {
    const value: any;
    export = value;
}

declare interface IWordCard {
    mean: string;
    word: string;
    openCount: number;
    isCompleted: boolean;
}