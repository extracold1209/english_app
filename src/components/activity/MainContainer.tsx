import React, { Component } from 'react';
import FooterSection from '../sections/FooterSection';
import { Container } from 'native-base';
import HeaderTabSection from '../sections/HeaderTabSection';
import TotalWordSection from '../sections/TotalWordSection';
import LackWordSection from '../sections/LackWordSection';
import CompletedWordSection from '../sections/CompletedWordSection';
import { NavigationScreenProps } from 'react-navigation';

interface IProps extends NavigationScreenProps {}

export default class MainContainer extends Component<IProps> {
    render() {
        return (
            <Container>
                <HeaderTabSection
                    tabs={[
                        { name: '단어장', component: <TotalWordSection /> },
                        { name: '부족한단어', component: <LackWordSection /> },
                        { name: '다외운단어', component: <CompletedWordSection /> }
                    ]}
                />
                <FooterSection />
            </Container>
        );
    }
}