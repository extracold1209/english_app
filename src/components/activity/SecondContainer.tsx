import React, { Component } from 'react';
import { Container, Content } from 'native-base';
import { Text } from 'react-native';

export default class SecondContainer extends Component<{ navigation: any }> {
    render() {
        return (
            <Container>
                <Content>
                    <Text>여기가 즐찾이 될것</Text>
                </Content>
            </Container >
        );
    }
}