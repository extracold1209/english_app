import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Content, Body, Text, Grid, Col } from 'native-base';
import { FlatList } from 'react-native';
import WordCard from '../atoms/WordCard';
import { IStoreState } from '../../store/modules'

interface IProps {
    wordList: IWordCard[],
}

class LackWordSection extends Component<IProps> {
    private makeNoContentPage() {
        return (
            <Body>
                <Text>{`오 모르는게 업내용 >_<`}</Text>
            </Body>
        );
    }

    private makeCardList() {
        const { wordList } = this.props;
        return (
            <FlatList
                data={wordList.filter((cardItem) => (cardItem.openCount >= 1 && !cardItem.isCompleted))}
                renderItem={({ item }) => <WordCard {...item} />}
                keyExtractor={(item) => item.word + item.mean}
            />
        );
    }

    render() {
        const { wordList } = this.props;

        return (
            <Content padder>
                {wordList.filter((cardItem) => (cardItem.openCount >= 1)).length === 0 ? this.makeNoContentPage() : this.makeCardList()}
            </Content >
        );
    }
}

const mapStateToProps = (state: IStoreState) => ({
    wordList: state.words.wordList,
});
const mapDispatchToProps = undefined;

export default connect(mapStateToProps, mapDispatchToProps)(LackWordSection);