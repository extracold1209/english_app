import React, { Component } from 'react';
import { Header, Title, Body, Left, Button, Icon, Right } from 'native-base';
import { connect } from 'react-redux';
import { IStoreState } from '../../store/modules';
import { IColorState } from '../../store/modules/colors';
import { NavigationScreenProps, NavigationActions, DrawerActions } from 'react-navigation';

interface IProps extends NavigationScreenProps {
    colorSet: IColorState,
}

class HeaderSection extends Component<IProps> {
    private drawer: any;

    render() {
        const { primary, primaryDark } = this.props.colorSet;
        return (

            <Header
                hasTabs
                androidStatusBarColor={primaryDark}
                style={{ backgroundColor: primary }}
            >
                <Left>
                    <Button transparent>
                        <Icon
                            name='menu'
                            onPress={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer()) }}
                        />
                    </Button>
                </Left>
                <Body>
                    <Title>가영이의 영단어장</Title>
                </Body>
                {/* Right View 가 없는 경우, Body 가 가운데정렬된다. */}
                <Right></Right>
            </Header>
        );
    }
}

const mapStateToProps = (state: IStoreState) => ({
    colorSet: state.color,
});
const mapDispatchToProps = undefined;

export default connect(mapStateToProps, mapDispatchToProps)(HeaderSection);