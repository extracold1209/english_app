import React, { Component } from 'react';
import { Tab, Tabs } from 'native-base';

interface IProps {
  tabs: {
    name: string,
    component: Element,
  }[]
}

export default class HeaderTabSection extends Component<IProps> {
  render() {
    return (
      <Tabs
        locked={true}
      >
        {
          this.props.tabs.map(({ name, component }) => (
            <Tab
              activeTabStyle={{ backgroundColor: '#79CC46' }}
              tabStyle={{ backgroundColor: '#79CC46' }}
              textStyle={{ color: '#FFFFFF' }}
              heading={name}
              key={name}
            >
              {component}
            </Tab>
          ))
        }
      </Tabs>
    );
  }
}