import React, { Component } from 'react';
import { Footer, FooterTab, Button, Text } from 'native-base';
import { Alert } from 'react-native';
import { connect } from 'react-redux';
import { IStoreState } from '../../store/modules';
import { IColorState } from '../../store/modules/colors';

interface IProps {
    colorSet: IColorState,
}


class FooterSection extends Component<IProps> {
    render() {
        return (
            <Footer>
                <FooterTab>
                    <Button full style={{ backgroundColor: this.props.colorSet.primary }}>
                        <Text
                            style={{ color: '#FFFFFF', fontSize: 14 }}
                            onPress={() => { Alert.alert('피드백 주세용', '사랑합니당 ><♡') }}
                        >이기웅 올림</Text>
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}


const mapStateToProps = (state: IStoreState) => ({
    colorSet: state.color,
});
const mapDispatchToProps = undefined;

export default connect(mapStateToProps, mapDispatchToProps)(FooterSection);