import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Content, Text, Button, SwipeRow, Icon } from 'native-base';
import { FlatList, Alert } from 'react-native';
import WordCard from '../atoms/WordCard';
import { IStoreState } from '../../store/modules'

interface IProps {
    wordList: IWordCard[],
}

class TotalWordSection extends Component<IProps> {
    render() {
        const { wordList } = this.props;

        return (
            <Content padder>
                <FlatList
                    data={wordList.filter((cardItem) => cardItem.isCompleted)}
                    renderItem={({ item }) => <WordCard
                        {...item}
                        fixOpenState={'open'}
                    />}
                    keyExtractor={(item) => item.word + item.mean}
                />
            </Content >
        );
    }
}

const mapStateToProps = (state: IStoreState) => ({
    wordList: state.words.wordList,
});
const mapDispatchToProps = undefined;

export default connect(mapStateToProps, mapDispatchToProps)(TotalWordSection);