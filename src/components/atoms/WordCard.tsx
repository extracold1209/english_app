import React, { Component } from 'react';
import { Card, CardItem, Body, Text, Right, Icon, SwipeRow } from 'native-base';
import Tts from 'react-native-tts';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { WordActionCreators } from '../../store/modules/word';

type OwnProps = IWordCard;
type StateProps = {
    increaseOpenCount: (word: string) => void,
    completeWord: (word: string) => void,
}
type IProps = OwnProps & StateProps & {
    fixOpenState?: 'open' | 'close';
};

interface IState {
    isCardOpened: boolean;
}

class WordCard extends Component<IProps, IState> {
    private component: any; // SwipeRow 에 등록되지 않은 코드를 사용하기 위함
    private swipeValueThreshold = 75;

    constructor(props: Readonly<IProps>) {
        super(props);

        let initialState = false;
        if (this.props.fixOpenState === 'open') {
            initialState = true;
        }

        this.state = {
            isCardOpened: initialState,
        }
    }

    /**
     * 카드를 오픈 상태를 토글. 현재 상태가 열린 경우가 아니면 부족한단어로 이동
     * fixOpenState 가 정의되어있는 경우 (true, false) 카드 오픈수와 토글은 동작하지 않는다.
     */
    toggleCardOpen() {
        if (this.props.fixOpenState) {
            return;
        }

        if (!this.state.isCardOpened) {
            const { increaseOpenCount, word } = this.props;
            increaseOpenCount(word);
        }

        this.setState({
            isCardOpened: !this.state.isCardOpened,
        })
    }

    makeCardHeader() {
        const { openCount, word } = this.props;
        const { isCardOpened } = this.state;

        return (
            <CardItem header button onPress={() => { this.toggleCardOpen(); }}>
                <Body><Text>{word}</Text></Body>
                {isCardOpened &&
                    <Right
                        style={{ paddingRight: 12 }}
                    >
                        <Text>{`${openCount}회 확인`}</Text>
                    </Right>
                }
            </CardItem>
        )
    }

    makeCardContent() {
        const { mean, word } = this.props;

        return (
            <CardItem>
                <Body><Text>{mean}</Text></Body>
                <Icon
                    name='volume-high'
                    onPress={() => { Tts.speak(word); }}
                />
                <Icon
                    name='star-outline'
                    onPress={() => { }}
                />
            </CardItem>
        )
    }

    render() {
        const { isCardOpened } = this.state;
        const { fixOpenState, completeWord, word } = this.props;

        return (
            <SwipeRow
                disableLeftSwipe={fixOpenState !== undefined}
                disableRightSwipe={fixOpenState !== undefined}
                ref={(element) => { if (element !== null) { this.component = element } }}
                rightOpenValue={this.swipeValueThreshold * -1}
                leftOpenValue={this.swipeValueThreshold}
                onRowOpen={() => {
                    if (this.component) {
                        this.component._root.closeRow();
                        completeWord(word);
                    }
                }}
                style={{
                    borderBottomWidth: 0,
                }}
                body={
                    <Card
                        style={{
                            marginTop: -10,
                            marginBottom: -10,
                            width: '100%',
                        }}
                    >
                        {this.makeCardHeader()}
                        {isCardOpened && this.makeCardContent()}
                    </Card>
                }
            />
        );
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    ...bindActionCreators(WordActionCreators, dispatch),
})

export default connect(undefined, mapDispatchToProps)(WordCard);