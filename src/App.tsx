import React, { Component } from 'react';
import MainContainer from './components/activity/MainContainer';
import SecondContainer from './components/activity/SecondContainer';
import SplashContainer from './components/activity/SplashContainer';
import { Provider } from 'react-redux';
import { createAppContainer, createStackNavigator, createDrawerNavigator } from 'react-navigation';
import getStore from './store';
import HeaderSection from './components/sections/HeaderSection';
import { PersistGate } from 'redux-persist/integration/react';

interface Props { }

const DrawerNavigator = createDrawerNavigator(
  {
    Home: MainContainer,
    Second: SecondContainer,
  }, {
    initialRouteName: 'Home',
    drawerLockMode: 'locked-closed',
    resetOnBlur: true
  }
);

const DefaultStackNavigation = createStackNavigator(
  {
    Drawer: DrawerNavigator,
  }, {
    initialRouteName: 'Drawer',
    defaultNavigationOptions: ({ navigation }) => ({
      header: <HeaderSection navigation={navigation} />
    })
  }
)

const AppNavigator = createAppContainer(
  createStackNavigator(
    {
      Default: DefaultStackNavigation,
    },
    {
      initialRouteName: 'Default',
      headerMode: 'none',
    }
  )
);

const { store, persistor } = getStore();

export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <AppNavigator />
        </PersistGate>
      </Provider>
    );
  }
}