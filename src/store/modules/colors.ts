import { createAction, handleActions } from 'redux-actions';

// types
const CHANGE_COLOR = 'colors/CHANGE_COLOR';

// actions
export const changeColorSet = createAction<IWordCard, IWordCard>(CHANGE_COLOR, (item: IWordCard) => item);

// states
export interface IColorState {
    primary: string;
    primaryDark: string;
}

const defaultState: IColorState = {
    primary: '#06CC6B',
    primaryDark: '#454F24',
}

// reducer
export default handleActions<IColorState, IColorState>({
    [CHANGE_COLOR]: (colorSet, { payload }) => (payload),
}, defaultState)