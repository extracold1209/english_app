import { combineReducers } from 'redux';
import wordReducer, { IWordState } from './word';
import colorReducer, { IColorState } from './colors';


export default combineReducers<IStoreState>({
    words: wordReducer,
    color: colorReducer,
});

// 스토어의 상태 타입 정의
export interface IStoreState {
    words: IWordState
    color: IColorState
}