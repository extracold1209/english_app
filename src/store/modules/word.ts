import { createAction, handleActions } from 'redux-actions';
import { produce, Produced } from 'immer';


// types
const INCREASE_OPENCOUNT = 'word/INCREASE_OPENCOUNT';
const COMPLETE_WORD = 'word/COMPLETE_WORD';

// actions
type IncreaseOpenCountActionPayload = string;
type CompleteWordActionPayload = string;

export const WordActionCreators = {
    increaseOpenCount: createAction<string, IncreaseOpenCountActionPayload>(INCREASE_OPENCOUNT, (word: string) => word),
    completeWord: createAction<string, CompleteWordActionPayload>(COMPLETE_WORD, (word: string) => word)
};

// states
export interface IWordState {
    wordList: IWordCard[],
}

const defaultState: IWordState = {
    wordList: [
        { word: 'damagingly', mean: '피해가 되게' },
        { word: 'vacant', mean: '공석의, 비어있는' },
        { word: 'deciding', mean: '결정적인' },
        { word: 'vacate', mean: '비우다, 떠나다' },
        { word: 'decisive', mean: '결정적인' },
        { word: 'valor', mean: '용기, 용맹' },
        { word: 'ideally', mean: '더할나위없이, 완벽하게' },
        { word: 'accomplice', mean: '공범자' },
        { word: 'lasting', mean: '지속적인' },
        { word: 'accord', mean: '일치, 조화, 합의' },
        { word: 'namely', mean: '다시 말하면' },
        { word: 'account', mean: '설명, 계좌' },
        { word: 'safekeeping', mean: '보관, 보호' },
        { word: 'accountable', mean: '설명할 수 있는' },
        { word: 'seasonal', mean: '계절적인, 주기적인' },
        { word: 'accountably', mean: '설명할 수 있도록' },
        { word: 'seasoning', mean: '조미료' },
        { word: 'accounts payable department', mean: '미불금 부서' },
    ].map((item) => Object.assign(item, { openCount: 0, isCompleted: false }))
}

// reducer
export default handleActions<IWordState, IncreaseOpenCountActionPayload>({
    [INCREASE_OPENCOUNT]: ({ wordList }, { payload: word }) => ({
        wordList: produce(wordList, (draft) => {
            const selectedWordCard = draft.find((wordCard) => (wordCard.word === word));
            if (selectedWordCard) {
                selectedWordCard.openCount++;
            }
        })
    }),
    [COMPLETE_WORD]: ({ wordList }, { payload: word }) => ({
        wordList: produce(wordList, (draft) => {
            const selectedWordCard = draft.find((wordCard) => (wordCard.word === word));
            if (selectedWordCard) {
                selectedWordCard.isCompleted = true;
            }
        })
    })
}, defaultState)